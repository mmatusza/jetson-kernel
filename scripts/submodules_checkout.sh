#!/bin/bash

ROOT_P=$(realpath $(dirname "$0"))/../
REF=tegra-l4t-r28.2
echo "ROOT_P: ${ROOT_P}"

cd ${ROOT_P}kernel/t18x; git checkout ${REF}
exit
cd ${ROOT_P}kernel/kernel-4.4; git checkout ${REF}
cd ${ROOT_P}kernel/nvgpu; git checkout ${REF}
cd ${ROOT_P}kernel/nvhost; git checkout ${REF}
cd ${ROOT_P}kernel/nvmap; git checkout ${REF}
cd ${ROOT_P}kernel/nvmap-t18x; git checkout ${REF}
cd ${ROOT_P}kernel/display; git checkout ${REF}
cd ${ROOT_P}hardware/nvidia/soc/t18x; git checkout ${REF}
cd ${ROOT_P}hardware/nvidia/platform/tegra/common; git checkout ${REF}
cd ${ROOT_P}hardware/nvidia/platform/t18x/common; git checkout ${REF}
cd ${ROOT_P}hardware/nvidia/platform/t18x/quill; git checkout ${REF}
cd ${ROOT_P}hardware/nvidia/soc/t210; git checkout ${REF}
cd ${ROOT_P}hardware/nvidia/platform/t210/common; git checkout ${REF}
cd ${ROOT_P}hardware/nvidia/platform/t210/jetson; git checkout ${REF}
cd ${ROOT_P}hardware/nvidia/soc/tegra; git checkout ${REF}
