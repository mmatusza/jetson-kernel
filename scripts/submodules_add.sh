#!/bin/bash

ROOT_P=$(dirname "$0")/../
echo "ROOT_P: ${ROOT_P}"

git submodule add --force -b master --name kernel/kernel-4.4 git://nv-tegra.nvidia.com/linux-4.4.git kernel/kernel-4.4
git submodule add --force -b master --name kernel/t18x git://nv-tegra.nvidia.com/linux-t18x.git kernel/t18x
git submodule add --force -b master --name kernel/nvgpu git://nv-tegra.nvidia.com/linux-nvgpu.git kernel/nvgpu
git submodule add --force -b master --name kernel/nvhost git://nv-tegra.nvidia.com/linux-nvhost.git kernel/nvhost
git submodule add --force -b master --name kernel/nvmap git://nv-tegra.nvidia.com/linux-nvmap.git kernel/nvmap
git submodule add --force -b master --name kernel/nvmap-t18x git://nv-tegra.nvidia.com/linux-nvmap-t18x.git kernel/nvmap-t18x
git submodule add --force -b master --name kernel/display git://nv-tegra.nvidia.com/linux-display.git kernel/display
git submodule add --force -b master --name hardware/nvidia/soc/t18x git://nv-tegra.nvidia.com/device/hardware/nvidia/soc/t18x.git hardware/nvidia/soc/t18x
git submodule add --force -b master --name hardware/nvidia/platform/tegra/common git://nv-tegra.nvidia.com/device/hardware/nvidia/platform/tegra/common.git hardware/nvidia/platform/tegra/common
git submodule add --force -b master --name hardware/nvidia/platform/t18x/common git://nv-tegra.nvidia.com/device/hardware/nvidia/platform/t18x/common.git hardware/nvidia/platform/t18x/common
git submodule add --force -b master --name hardware/nvidia/platform/t18x/quill git://nv-tegra.nvidia.com/device/hardware/nvidia/platform/t18x/quill.git hardware/nvidia/platform/t18x/quill
git submodule add --force -b master --name hardware/nvidia/soc/t210 git://nv-tegra.nvidia.com/device/hardware/nvidia/soc/t210.git hardware/nvidia/soc/t210
git submodule add --force -b master --name hardware/nvidia/platform/t210/common git://nv-tegra.nvidia.com/device/hardware/nvidia/platform/t210/common.git hardware/nvidia/platform/t210/common
git submodule add --force -b master --name hardware/nvidia/platform/t210/jetson git://nv-tegra.nvidia.com/device/hardware/nvidia/platform/t210/jetson.git hardware/nvidia/platform/t210/jetson
git submodule add --force -b master --name hardware/nvidia/soc/tegra git://nv-tegra.nvidia.com/device/hardware/nvidia/soc/tegra.git hardware/nvidia/soc/tegra
