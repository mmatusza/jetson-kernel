#!/usr/bin/env bash

#
# Setup Environment
#

. $(dirname $0)/environment.sh

cd $TEGRA_KERNEL_SRC

AUV_KERNEL_VERSION=`make O=$AUV_KERNEL_OUT kernelversion`
echo "Build name: $AUV_KERNEL_VERSION"

echo "Building zImage"
make O=$TEGRA_KERNEL_OUT zImage -j8

echo "Building DTBs"
make O=$TEGRA_KERNEL_OUT dtbs 

echo "Building modules"
make O=$TEGRA_KERNEL_OUT modules -j8

#return to origin
cd $DEVDIR
