#!/usr/bin/env bash

#
# Setup Environment
#

export AUV_BUILD_NAME=J120_2.2.0

export DEVDIR=/data/git/jetson-kernel_
export LINARO_DIR=/data/tools/linaro
export USER=maciej

# tools for r24.2.1
export CROSS_COMPILE=$LINARO_DIR/gcc-linaro-5.3-2016.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
export CROSS32CC=$LINARO_DIR/gcc-linaro-5.3-2016.02-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-gcc


export JETPACK=/data/JetPack3.2
export TEGRA_KERNEL_SRC=${DEVDIR}/sources/kernel/kernel-4.4
export KERNEL_INITRD=${DEVDIR}/initrd
export TEGRA_KERNEL_OUT=${DEVDIR}/images
export KERNEL_MODULES_OUT=$TEGRA_KERNEL_OUT/modules
export ARCH=arm64

echo "# ENVIRONMENT"

echo "LINARO DIR:         $LINARO_DIR"
echo "JETPACK 3.0:        $JETPACK"
echo "KERNEL SOURCE:      $TEGRA_KERNEL_SRC"
echo "OUTPUT DIR:         $TEGRA_KERNEL_OUT"
echo "PACKAGE_DIR:        $KERNEL_MODULES_OUT"
echo "INITRD:             $KERNEL_INITRD"



