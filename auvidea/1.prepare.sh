#!/usr/bin/env bash

#
# Setup Environment
#

. $(dirname $0)/environment.sh


#
# Clean the environment
#
echo "cleaning the build and package dirs"
rm -rf $TEGRA_KERNEL_OUT

mkdir -p $TEGRA_KERNEL_OUT/modules 
mkdir -p $TEGRA_KERNEL_OUT/packages 

echo "cleaning up git sources"
#git checkout auvidea_j120 --force
#git checkout - .

cd $TEGRA_KERNEL_SRC


make mrproper 
make O=$TEGRA_KERNEL_OUT mrproper

cd $DEVDIR
