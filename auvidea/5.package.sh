#!/usr/bin/env bash

#
# Setup Environment
#

. $(dirname $0)/environment.sh

pushd $TEGRA_KERNEL_SRC

export KERNEL_MODULES_NAME=`make  --no-print-directory O=$TEGRA_KERNEL_OUT kernelrelease`
echo "kernelrelease:$AUV_KERNEL_REL"

echo "Installing modules to:$AUV_KERNEL_PACKAGE"
make O=$TEGRA_KERNEL_OUT modules_install INSTALL_MOD_PATH=$KERNEL_MODULES_OUT

echo "Cleaning build and source sn links in: $KERNEL_MODULES_OUT/lib/modules/$KERNEL_MODULES_NAME"
#clean links to build and sources
rm $KERNEL_MODULES_OUT/lib/modules/$KERNEL_MODULES_NAME/build
rm $KERNEL_MODULES_OUT/lib/modules/$KERNEL_MODULES_NAME/source


echo "Creating kernel_supplements"
cd $KERNEL_MODULES_OUT
tar -cjf kernel_supplements.tbz2 *
mv kernel_supplements.tbz2 $DEVDIR/images/packages

cd $DEVDIR
