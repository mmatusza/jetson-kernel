J120 specific software v. 1.1:
July 14. 2016
- auvidea_j120.dtb config file
- kernel files
- library files

Adds support for:
- M.2 type M slot
- IMU (via SPI bus)
- CAN interface (only CAN2, the SPI to CAN controller for CAN1 must be removed)
- B102 HDMI to CSI-2 bridge (1080p60, 1080p50, 720p60, 720p50, 640x480p60)

CAN:
can.sh loads the modules for CAN.
Please check with ifconfig whether they installed right.

Requirement:
Release 23.2 must be installed first

Changes:
- dtb file now auvidea_j120.dtb for J120 board (no CAN1 !)
- extlinux.conf changed for booting correct dtb file
- kernel uname is original NVidia name
- added CONFIG_BLK_DEV_NVME
- added MCP driver and modification for dtb
- added can support
- added spidev support
- added support for TC358743 (B102)

Installation:
- copy J120.tar.gz file to the Jetson TX1 installed on J120 carrier board (e.g. copy file to the desktop)
- unpack J120.tar.gz (just by double clicking the file symbol on the desktop)
- open terminal
- backup the /boot and /lib folders
- go into extracted folder
- sudo cp -rf Image /boot
- sudo cp -rf zImage /boot
- sudo cp -rf auvidea_j120.dtb /boot
- sudo cp -rf /home/ubuntu/Desktop/kernel_new/lib/* /lib
- verify that all files copied correctly
- edit extlinux.conf to use the auvidea_j120.dtb file
- reboot your Jetson TX1

Disclaimer:
This software is provided without any warranty. Please use it at your own risk.