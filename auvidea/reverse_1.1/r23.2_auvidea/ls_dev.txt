total 4
drwxr-xr-x  12 root   root        4900 Mar 27 11:06 ./
drwxr-xr-x  21 root   root        4096 Mar 21 23:35 ../
crw-------   1 root   root    237,   0 Mar 27 11:06 adnc0
crw-------   1 root   root    237,   1 Mar 27 11:06 adnc1
crw-------   1 root   root    237,   2 Mar 27 11:06 adnc2
crw-------   1 root   root    237,   3 Mar 27 11:06 adnc3
crw-------   1 root   root    237,   4 Mar 27 11:06 adnc4
crw-------   1 root   root    237,   5 Mar 27 11:06 adnc5
drwxr-xr-x   2 root   root        1000 Mar 27 11:06 block/
drwxr-xr-x   3 root   root          60 Jan  1  1970 bus/
drwxr-xr-x   2 root   root        3780 Mar 27 11:06 char/
crw-------   1 root   root      5,   1 Mar 27 11:06 console
crw-------   1 root   root     10,  38 Mar 27 11:06 constraint_cpu_freq
crw-------   1 root   root     10,  37 Mar 27 11:06 constraint_gpu_freq
crw-------   1 root   root     10,  36 Mar 27 11:06 constraint_online_cpus
lrwxrwxrwx   1 root   root          11 Mar 27 11:06 core -> /proc/kcore
crw-------   1 root   root     10,  50 Mar 27 11:06 cpu_dma_latency
crw-------   1 root   root     10,  44 Mar 27 11:06 cpu_freq_max
crw-------   1 root   root     10,  45 Mar 27 11:06 cpu_freq_min
drwxr-xr-x   7 root   root         140 Mar 27 11:06 disk/
crw-------   1 root   root     10,  41 Mar 27 11:06 emc_freq_min
crw-rw----   1 root   video    29,   0 Mar 27 11:06 fb0
lrwxrwxrwx   1 root   root          13 Mar 27 11:06 fd -> /proc/self/fd/
crw-rw-rw-   1 root   root      1,   7 Mar 27 11:06 full
crw-rw-rw-   1 root   root     10, 229 Mar 27 11:06 fuse
crw-------   1 root   root     10,  42 Mar 27 11:06 gpu_freq_max
crw-------   1 root   root     10,  43 Mar 27 11:06 gpu_freq_min
crw-------   1 root   root     89,   0 Mar 27 11:06 i2c-0
crw-------   1 root   root     89,   1 Mar 27 11:06 i2c-1
crw-------   1 root   root     89,   2 Mar 27 11:06 i2c-2
crw-------   1 root   root     89,   3 Mar 27 11:06 i2c-3
crw-------   1 root   root     89,   4 Mar 27 11:06 i2c-4
crw-------   1 root   root     89,   5 Mar 27 11:06 i2c-5
crw-------   1 root   root     89,   6 Mar 27 11:06 i2c-6
crw-------   1 root   root    250,   0 Mar 27 11:06 iio:device0
drwxr-xr-x   3 root   root         100 Mar 27 11:06 input/
crw-------   1 root   root     10,  56 Mar 27 11:06 keychord
crw-r--r--   1 root   root      1,  11 Mar 27 11:06 kmsg
crw-------   1 root   root     10,  52 Mar 27 11:06 last_trc
srw-rw-rw-   1 root   root           0 Mar 27 11:06 log=
brw-rw----   1 root   disk      7,   0 Mar 27 11:06 loop0
brw-rw----   1 root   disk      7,   1 Mar 27 11:06 loop1
brw-rw----   1 root   disk      7,   2 Mar 27 11:06 loop2
brw-rw----   1 root   disk      7,   3 Mar 27 11:06 loop3
brw-rw----   1 root   disk      7,   4 Mar 27 11:06 loop4
brw-rw----   1 root   disk      7,   5 Mar 27 11:06 loop5
brw-rw----   1 root   disk      7,   6 Mar 27 11:06 loop6
brw-rw----   1 root   disk      7,   7 Mar 27 11:06 loop7
crw-------   1 root   root     10, 237 Mar 27 11:06 loop-control
drwxr-xr-x   2 root   root          60 Jan  1  1970 mapper/
crw-------   1 root   root     10,  40 Mar 27 11:06 max_cpu_power
crw-------   1 root   root     10,  39 Mar 27 11:06 max_gpu_power
crw-------   1 root   root     10,  46 Mar 27 11:06 max_online_cpus
brw-rw----   1 root   disk      9,   0 Mar 27 11:06 md0
crw-r-----   1 root   kmem      1,   1 Mar 27 11:06 mem
crw-------   1 root   root     10,  47 Mar 27 11:06 min_online_cpus
crw-rw----   1 root   video    10,  54 Mar 27 11:06 mipi-cal
brw-rw----   1 root   disk    179,   0 Mar 27 11:06 mmcblk0
brw-rw----   1 root   disk    179,   1 Mar 27 11:06 mmcblk0p1
brw-rw----   1 root   disk    179,  10 Mar 27 11:06 mmcblk0p10
brw-rw----   1 root   disk    179,  11 Mar 27 11:06 mmcblk0p11
brw-rw----   1 root   disk    179,  12 Mar 27 11:06 mmcblk0p12
brw-rw----   1 root   disk    179,  13 Mar 27 11:06 mmcblk0p13
brw-rw----   1 root   disk    179,  14 Mar 27 11:06 mmcblk0p14
brw-rw----   1 root   disk    179,  15 Mar 27 11:06 mmcblk0p15
brw-rw----   1 root   disk    259,   0 Mar 27 11:06 mmcblk0p16
brw-rw----   1 root   disk    259,   1 Mar 27 11:06 mmcblk0p17
brw-rw----   1 root   disk    259,   2 Mar 27 11:06 mmcblk0p18
brw-rw----   1 root   disk    179,   2 Mar 27 11:06 mmcblk0p2
brw-rw----   1 root   disk    179,   3 Mar 27 11:06 mmcblk0p3
brw-rw----   1 root   disk    179,   4 Mar 27 11:06 mmcblk0p4
brw-rw----   1 root   disk    179,   5 Mar 27 11:06 mmcblk0p5
brw-rw----   1 root   disk    179,   6 Mar 27 11:06 mmcblk0p6
brw-rw----   1 root   disk    179,   7 Mar 27 11:06 mmcblk0p7
brw-rw----   1 root   disk    179,   8 Mar 27 11:06 mmcblk0p8
brw-rw----   1 root   disk    179,   9 Mar 27 11:06 mmcblk0p9
brw-rw----   1 root   disk    179,  16 Mar 27 11:06 mmcblk0rpmb
brw-rw----   1 root   disk    179,  32 Mar 27 11:06 mmcblk1
brw-rw----   1 root   disk    179,  33 Mar 27 11:06 mmcblk1p1
drwxr-xr-x   2 root   root          60 Jan  1  1970 net/
crw-------   1 root   root     10,  49 Mar 27 11:06 network_latency
crw-------   1 root   root     10,  48 Mar 27 11:06 network_throughput
crw-rw-rw-   1 root   root      1,   3 Mar 27 11:06 null
crw-rw----   1 root   video   240,   1 Mar 27 11:06 nvhost-as-gpu
crw-rw----   1 root   video   249,   0 Mar 27 11:06 nvhost-ctrl
crw-rw----   1 root   video   240,   2 Mar 27 11:06 nvhost-ctrl-gpu
crw-rw----   1 root   video   236,   1 Mar 27 11:06 nvhost-ctrl-isp
crw-rw----   1 root   video   235,   1 Mar 27 11:06 nvhost-ctrl-isp.1
crw-rw----   1 root   video   246,   1 Mar 27 11:06 nvhost-ctrl-nvdec
crw-rw----   1 root   video   234,   1 Mar 27 11:06 nvhost-ctrl-vi
crw-rw----   1 root   video   240,   3 Mar 27 11:06 nvhost-dbg-gpu
crw-rw----   1 root   video   240,   0 Mar 27 11:06 nvhost-gpu
crw-rw----   1 root   video   236,   0 Mar 27 11:06 nvhost-isp
crw-rw----   1 root   video   235,   0 Mar 27 11:06 nvhost-isp.1
crw-rw----   1 root   video   244,   0 Mar 27 11:06 nvhost-msenc
crw-rw----   1 root   video   246,   0 Mar 27 11:06 nvhost-nvdec
crw-rw----   1 root   video   243,   0 Mar 27 11:06 nvhost-nvjpg
crw-rw----   1 root   video   240,   4 Mar 27 11:06 nvhost-prof-gpu
crw-rw----   1 root   video   248,   0 Mar 27 11:06 nvhost-tsec
crw-rw----   1 root   video   247,   0 Mar 27 11:06 nvhost-tsecb
crw-rw----   1 root   video   240,   5 Mar 27 11:06 nvhost-tsg-gpu
crw-rw----   1 root   video   234,   0 Mar 27 11:06 nvhost-vi
crw-rw----   1 root   video   245,   0 Mar 27 11:06 nvhost-vic
crw-rw-rw-   1 root   root    195, 255 Mar 27 11:06 nvidiactl
crw-rw----   1 root   video    10,  61 Mar 27 11:06 nvmap
crw-r-----   1 root   kmem      1,   4 Mar 27 11:06 port
crw-------   1 root   root    108,   0 Mar 27 11:06 ppp
crw-rw-rw-   1 root   tty       5,   2 Mar 27 11:08 ptmx
drwxr-xr-x   2 root   root           0 Jan  1  1970 pts/
crw-------   1 root   root     10,  59 Mar 27 11:06 quadd
crw-------   1 root   root     10,  58 Mar 27 11:06 quadd_auth
brw-rw----   1 root   disk      1,   0 Mar 27 11:06 ram0
brw-rw----   1 root   disk      1,   1 Mar 27 11:06 ram1
brw-rw----   1 root   disk      1,  10 Mar 27 11:06 ram10
brw-rw----   1 root   disk      1,  11 Mar 27 11:06 ram11
brw-rw----   1 root   disk      1,  12 Mar 27 11:06 ram12
brw-rw----   1 root   disk      1,  13 Mar 27 11:06 ram13
brw-rw----   1 root   disk      1,  14 Mar 27 11:06 ram14
brw-rw----   1 root   disk      1,  15 Mar 27 11:06 ram15
brw-rw----   1 root   disk      1,   2 Mar 27 11:06 ram2
brw-rw----   1 root   disk      1,   3 Mar 27 11:06 ram3
brw-rw----   1 root   disk      1,   4 Mar 27 11:06 ram4
brw-rw----   1 root   disk      1,   5 Mar 27 11:06 ram5
brw-rw----   1 root   disk      1,   6 Mar 27 11:06 ram6
brw-rw----   1 root   disk      1,   7 Mar 27 11:06 ram7
brw-rw----   1 root   disk      1,   8 Mar 27 11:06 ram8
brw-rw----   1 root   disk      1,   9 Mar 27 11:06 ram9
crw-rw-rw-   1 root   root      1,   8 Mar 27 11:06 random
crw-rw-r--+  1 root   root     10,  62 Mar 27 11:06 rfkill
lrwxrwxrwx   1 root   root           4 Mar 27 11:06 rtc -> rtc0
crw-------   1 root   root    254,   0 Mar 27 11:06 rtc0
lrwxrwxrwx   1 root   root           8 Mar 27 11:06 shm -> /run/shm/
drwxr-xr-x   3 root   root         120 Mar 27 11:06 snd/
crw-------   1 root   root    153,   0 Mar 27 11:06 spidev1.0
crw-------   1 root   root    153,   1 Mar 27 11:06 spidev1.1
crw-------   1 root   root    153,   2 Mar 27 11:06 spidev3.0
lrwxrwxrwx   1 root   root          15 Mar 27 11:06 stderr -> /proc/self/fd/2
lrwxrwxrwx   1 root   root          15 Mar 27 11:06 stdin -> /proc/self/fd/0
lrwxrwxrwx   1 root   root          15 Mar 27 11:06 stdout -> /proc/self/fd/1
crw-------   1 root   root     10,  55 Mar 27 11:06 sw_sync
crw-rw----   1 root   video    10,  60 Mar 27 11:06 tegra_camera_ctrl
crw-rw----   1 root   video    10,  35 Mar 27 11:06 tegra-crypto
crw-rw----   1 root   video   242,   1 Mar 27 11:06 tegra_dc_0
crw-rw----   1 root   video   242,   0 Mar 27 11:06 tegra_dc_ctrl
crw-rw----   1 root   video    10,   1 Mar 27 11:06 tegra-throughput
crw-------   1 root   root     10,  57 Mar 27 11:06 timerinfo
crw-------   1 root   root     10,  53 Mar 27 11:06 trc
crw-rw-rw-   1 root   tty       5,   0 Mar 27 11:06 tty
crw--w----   1 root   tty       4,   0 Mar 27 11:06 tty0
crw-rw----   1 root   tty       4,   1 Mar 27 11:06 tty1
crw--w----   1 root   tty       4,  10 Mar 27 11:06 tty10
crw--w----   1 root   tty       4,  11 Mar 27 11:06 tty11
crw--w----   1 root   tty       4,  12 Mar 27 11:06 tty12
crw--w----   1 root   tty       4,  13 Mar 27 11:06 tty13
crw--w----   1 root   tty       4,  14 Mar 27 11:06 tty14
crw--w----   1 root   tty       4,  15 Mar 27 11:06 tty15
crw--w----   1 root   tty       4,  16 Mar 27 11:06 tty16
crw--w----   1 root   tty       4,  17 Mar 27 11:06 tty17
crw--w----   1 root   tty       4,  18 Mar 27 11:06 tty18
crw--w----   1 root   tty       4,  19 Mar 27 11:06 tty19
crw-rw----   1 root   tty       4,   2 Mar 27 11:06 tty2
crw--w----   1 root   tty       4,  20 Mar 27 11:06 tty20
crw--w----   1 root   tty       4,  21 Mar 27 11:06 tty21
crw--w----   1 root   tty       4,  22 Mar 27 11:06 tty22
crw--w----   1 root   tty       4,  23 Mar 27 11:06 tty23
crw--w----   1 root   tty       4,  24 Mar 27 11:06 tty24
crw--w----   1 root   tty       4,  25 Mar 27 11:06 tty25
crw--w----   1 root   tty       4,  26 Mar 27 11:06 tty26
crw--w----   1 root   tty       4,  27 Mar 27 11:06 tty27
crw--w----   1 root   tty       4,  28 Mar 27 11:06 tty28
crw--w----   1 root   tty       4,  29 Mar 27 11:06 tty29
crw-rw----   1 root   tty       4,   3 Mar 27 11:06 tty3
crw--w----   1 root   tty       4,  30 Mar 27 11:06 tty30
crw--w----   1 root   tty       4,  31 Mar 27 11:06 tty31
crw--w----   1 root   tty       4,  32 Mar 27 11:06 tty32
crw--w----   1 root   tty       4,  33 Mar 27 11:06 tty33
crw--w----   1 root   tty       4,  34 Mar 27 11:06 tty34
crw--w----   1 root   tty       4,  35 Mar 27 11:06 tty35
crw--w----   1 root   tty       4,  36 Mar 27 11:06 tty36
crw--w----   1 root   tty       4,  37 Mar 27 11:06 tty37
crw--w----   1 root   tty       4,  38 Mar 27 11:06 tty38
crw--w----   1 root   tty       4,  39 Mar 27 11:06 tty39
crw-rw----   1 root   tty       4,   4 Mar 27 11:06 tty4
crw--w----   1 root   tty       4,  40 Mar 27 11:06 tty40
crw--w----   1 root   tty       4,  41 Mar 27 11:06 tty41
crw--w----   1 root   tty       4,  42 Mar 27 11:06 tty42
crw--w----   1 root   tty       4,  43 Mar 27 11:06 tty43
crw--w----   1 root   tty       4,  44 Mar 27 11:06 tty44
crw--w----   1 root   tty       4,  45 Mar 27 11:06 tty45
crw--w----   1 root   tty       4,  46 Mar 27 11:06 tty46
crw--w----   1 root   tty       4,  47 Mar 27 11:06 tty47
crw--w----   1 root   tty       4,  48 Mar 27 11:06 tty48
crw--w----   1 root   tty       4,  49 Mar 27 11:06 tty49
crw-rw----   1 root   tty       4,   5 Mar 27 11:06 tty5
crw--w----   1 root   tty       4,  50 Mar 27 11:06 tty50
crw--w----   1 root   tty       4,  51 Mar 27 11:06 tty51
crw--w----   1 root   tty       4,  52 Mar 27 11:06 tty52
crw--w----   1 root   tty       4,  53 Mar 27 11:06 tty53
crw--w----   1 root   tty       4,  54 Mar 27 11:06 tty54
crw--w----   1 root   tty       4,  55 Mar 27 11:06 tty55
crw--w----   1 root   tty       4,  56 Mar 27 11:06 tty56
crw--w----   1 root   tty       4,  57 Mar 27 11:06 tty57
crw--w----   1 root   tty       4,  58 Mar 27 11:06 tty58
crw--w----   1 root   tty       4,  59 Mar 27 11:06 tty59
crw-rw----   1 root   tty       4,   6 Mar 27 11:06 tty6
crw--w----   1 root   tty       4,  60 Mar 27 11:06 tty60
crw--w----   1 root   tty       4,  61 Mar 27 11:06 tty61
crw--w----   1 root   tty       4,  62 Mar 27 11:06 tty62
crw--w----   1 root   tty       4,  63 Mar 27 11:06 tty63
crw--w----   1 root   tty       4,   7 Mar 27 11:06 tty7
crw--w----   1 root   tty       4,   8 Mar 27 11:06 tty8
crw--w----   1 root   tty       4,   9 Mar 27 11:06 tty9
crw-------   1 ubuntu tty       4,  64 Mar 27 11:06 ttyS0
crw-rw----   1 root   dialout   4,  65 Mar 27 11:06 ttyS1
crw-rw----   1 root   dialout   4,  66 Mar 27 11:06 ttyS2
crw-rw----   1 root   dialout   4,  67 Mar 27 11:06 ttyS3
crw-rw----   1 root   dialout 241,   1 Mar 27 11:06 ttyTHS1
crw-rw----   1 root   dialout 241,   2 Mar 27 11:06 ttyTHS2
crw-rw----   1 root   dialout 241,   3 Mar 27 11:06 ttyTHS3
drwxr-xr-x   3 root   root          60 Mar 27 11:06 .udev/
crw-------   1 root   root     10, 239 Mar 27 11:06 uhid
crw-------   1 root   root     10, 223 Mar 27 11:06 uinput
crw-rw-rw-   1 root   root      1,   9 Mar 27 11:06 urandom
crw-rw----   1 root   tty       7,   0 Mar 27 11:06 vcs
crw-rw----   1 root   tty       7,   1 Mar 27 11:06 vcs1
crw-rw----   1 root   tty       7,   2 Mar 27 11:06 vcs2
crw-rw----   1 root   tty       7,   3 Mar 27 11:06 vcs3
crw-rw----   1 root   tty       7,   4 Mar 27 11:06 vcs4
crw-rw----   1 root   tty       7,   5 Mar 27 11:06 vcs5
crw-rw----   1 root   tty       7,   6 Mar 27 11:06 vcs6
crw-rw----   1 root   tty       7,   7 Mar 27 11:06 vcs7
crw-rw----   1 root   tty       7, 128 Mar 27 11:06 vcsa
crw-rw----   1 root   tty       7, 129 Mar 27 11:06 vcsa1
crw-rw----   1 root   tty       7, 130 Mar 27 11:06 vcsa2
crw-rw----   1 root   tty       7, 131 Mar 27 11:06 vcsa3
crw-rw----   1 root   tty       7, 132 Mar 27 11:06 vcsa4
crw-rw----   1 root   tty       7, 133 Mar 27 11:06 vcsa5
crw-rw----   1 root   tty       7, 134 Mar 27 11:06 vcsa6
crw-rw----   1 root   tty       7, 135 Mar 27 11:06 vcsa7
crw-------   1 root   root     10,  63 Mar 27 11:06 vga_arbiter
crw-------   1 root   root     10, 130 Mar 27 11:06 watchdog
crw-------   1 root   root    251,   0 Mar 27 11:06 watchdog0
crw-------   1 root   root     10,  51 Mar 27 11:06 xt_qtaguid
crw-rw-rw-   1 root   root      1,   5 Mar 27 11:06 zero
brw-rw----   1 root   disk    253,   0 Mar 27 11:06 zram0
