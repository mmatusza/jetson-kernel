#!/usr/bin/env bash

#
# Setup Environment
#

. $(dirname $0)/environment.sh

cd $TEGRA_KERNEL_SRC

#make the config base
echo "Building default config"
make O=$TEGRA_KERNEL_OUT tegra18_defconfig

# merge J120 config
echo "Merging config overrides: ./auvidea/config_override"
#./scripts/kconfig/merge_config.sh -n -r -O $AUV_KERNEL_OUT $AUV_KERNEL_OUT/.config ./auvidea/config_override

#somehow this mass up the build....
#sed -i.sed.org -r "s/^(CONFIG_LOCALVERSION=.*|# CONFIG_LOCALVERSION is not set)/CONFIG_LOCALVERSION=-$AUV_BUILD_NAME/" $AUV_KERNEL_OUT/.config

make O=$TEGRA_KERNEL_OUT menuconfig

make O=$TEGRA_KERNEL_OUT kernelversion

#return to origin
cd $DEVDIR
